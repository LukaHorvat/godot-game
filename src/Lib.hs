{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
module Lib (exports) where

import Control.Monad
import Data.Function ((&))
import qualified Data.Text as T
import Godot
import qualified Godot.Core.Label as Label
import qualified Godot.Core.RigidBody2D as RigidBody2D
import GHC.IO (unsafePerformIO)
import Godot.Gdnative (GodotFFI)
import Linear
import qualified Godot.Core.Node2D as Node2D
import Godot.Core.Node (get_viewport)
import qualified Godot.Core.Viewport as Viewport
import qualified Godot.Core.Input as Input

exports :: GdnativeHandle -> IO ()
exports desc = do
    registerClass $ RegClass desc $ classInit @Main

toGodot :: GodotFFI c high => high -> c
toGodot = unsafePerformIO . toLowLevel

fromGodot :: GodotFFI low c => low -> c
fromGodot = unsafePerformIO . fromLowLevel

data Main = Main
    { _mBase :: Node
    , _mTime :: MVar Float
    , _mFactor :: MVar Float
    }

instance HasBaseClass Main where
    type BaseClass Main = Node
    super = _mBase
instance NativeScript Main where
    classInit base = Main base <$> newMVar 0 <*> newMVar 0.1
    classMethods =
        [ method1 "_process" process
        , method0 "ready" ready
        ]
    classProperties  =
        [ createMVarProperty "factor" _mFactor (Left VariantTypeReal) ]

process :: Main -> GodotVariant -> IO ()
process self deltaVt = do
    return ()

ready :: Main -> IO ()
ready self = do
    return ()

deriveBase ''Main
